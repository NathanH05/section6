package com.imagescale.api.image;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import javax.imageio.ImageIO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.Resource;

public class ImageServiceTest {
  private Resource resource;
  private ResourceLoader resourceLoader;
  private ImageService service;

  @BeforeEach
  public void init() {
    this.resource = Mockito.mock(Resource.class);
    this.resourceLoader = Mockito.mock(ResourceLoader.class);
    this.service = new ImageService(this.resourceLoader);

    when(this.resourceLoader.getResource(anyString())).thenReturn(this.resource);
  }

  @Test
  public void getScaledImage_missingImage_shouldReturnEmpty() throws IOException {
    when(this.resource.exists()).thenReturn(false);

    Optional<byte[]> optional = this.service.getScaledImage("test.jpg", 200, 200);

    assertFalse(optional.isPresent());
  }

  @Test
  public void getScaledImage_existingImage_shouldReturnTheScaledImage() throws IOException {
    InputStream stream = getClass().getClassLoader().getResourceAsStream("images/test.jpg");

    when(this.resource.exists()).thenReturn(true);
    when(this.resource.getInputStream()).thenReturn(stream);

    Optional<byte[]> optional = this.service.getScaledImage("test.jpg", 100, 100);
    assertTrue(optional.isPresent());

    BufferedImage image = ImageIO.read(new ByteArrayInputStream(optional.get()));
    assertEquals(image.getWidth(), 100);
    assertEquals(image.getHeight(), 100);
  }
}
