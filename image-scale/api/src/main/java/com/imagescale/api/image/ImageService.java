package com.imagescale.api.image;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import javax.imageio.ImageIO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

@Service
public class ImageService {
  private ResourceLoader resourceLoader;

  @Autowired
  public ImageService(ResourceLoader resourceLoader) {
    this.resourceLoader = resourceLoader;
  }

  public Optional<byte[]> getScaledImage(String name, int w, int h) throws IOException {
    Resource resource = this.resourceLoader.getResource("classpath:images/" + name);
    if (!resource.exists()) {
      return Optional.empty();
    }

    BufferedImage image = ImageIO.read(resource.getInputStream());

    return Optional.of(ImageService.getBytes(ImageService.scale(image, w, h)));
  }

  private static byte[] getBytes(BufferedImage image) throws IOException {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    ImageIO.write(image, "jpg", stream);

    return stream.toByteArray();
  }

  private static BufferedImage scale(BufferedImage image, int w, int h) {
    BufferedImage scaledImage = new BufferedImage(w, h, image.getType());

    Graphics2D graphics2D = scaledImage.createGraphics();
    graphics2D.drawImage(image, 0, 0, w, h, null);
    graphics2D.dispose();

    return scaledImage;
  }
}
