package com.imagescale.api.image;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@RestController
public class ImageController {
  private ImageService service;

  @Autowired
  public ImageController(ImageService service) {
    this.service = service;
  }

  @RequestMapping(value = "/image/{name}", method = RequestMethod.GET,
      produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.APPLICATION_JSON_VALUE})
  public Object getImage(@PathVariable(value = "name") String name, @RequestParam("w") int w,
      @RequestParam("h") int h) throws IOException {

    return this.service.getScaledImage(name, w, h)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Image Not Found"));
  }
}
