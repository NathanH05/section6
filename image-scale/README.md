# image-scale

## Local Environment

### Requirements
 - Docker version 18+
 - Docker Compose version 1+

### Jenkins Blue Ocean

Usage:
- Create a new git repository in the image-scale directory: `git init`
- Commit the `image-scale` files: `git add . && git commit -m "Initial commit"`
- Run `docker-compose up`
- Go to `http://localhost:8081/blue`
- Enter the admin password that was displayed in the output of the `docker-compose` command
- Create a new `Git` pipeline with this value `/home/jenkins/image-scale` as the Repository URL
- Without entering credentials, click `Create Pipeline`
- A pipeline should now be running
